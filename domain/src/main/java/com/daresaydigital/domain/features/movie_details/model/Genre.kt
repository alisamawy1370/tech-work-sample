package com.daresaydigital.domain.features.movie_details.model

data class Genre(
    val id: Int,
    val name: String
)