package com.daresaydigital.domain.features.movie_details.model

data class BelongsToCollection(
    val backdropPath: String?,
    val id: Int,
    val name: String?,
    val posterPath: String?
)