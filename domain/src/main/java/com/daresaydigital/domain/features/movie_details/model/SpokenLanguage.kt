package com.daresaydigital.domain.features.movie_details.model

data class SpokenLanguage(
    val englishName: String?,
    val iso_639_1: String?,
    val name: String?
)