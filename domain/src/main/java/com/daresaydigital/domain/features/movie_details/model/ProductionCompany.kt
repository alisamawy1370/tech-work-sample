package com.daresaydigital.domain.features.movie_details.model

data class ProductionCompany(
    val id: Int,
    val logoPath: String?,
    val name: String?,
    val originCountry: String?
)