package com.daresaydigital.data.features.movie_details.model

data class SpokenLanguageDTO(
    val english_name: String?,
    val iso_639_1: String?,
    val name: String?
)