package com.daresaydigital.data.features.movie_details.model

data class ProductionCountryDTO(
    val iso_3166_1: String?,
    val name: String
)