package com.daresaydigital.data.features.movie_details.model

data class BelongsToCollectionDTO(
    val backdrop_path: String?,
    val id: Int,
    val name: String?,
    val poster_path: String?
)