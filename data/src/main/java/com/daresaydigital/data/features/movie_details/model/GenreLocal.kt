package com.daresaydigital.data.features.movie_details.model

data class GenreLocal(
    val id: Int,
    val name: String?
)