package com.daresaydigital.data.features.movie_details.model

data class ProductionCompanyDTO(
    val id: Int,
    val logo_path: String?,
    val name: String?,
    val origin_country: String?
)