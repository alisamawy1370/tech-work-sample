package com.daresaydigital.data.features.movie_details.model

data class GenreDTO(
    val id: Int,
    val name: String
)