package com.daresaydigital.tmdb

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DaresayApplication : Application() {

    //set config here
}